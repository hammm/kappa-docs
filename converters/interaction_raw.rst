Класс взаимодействий
--------------------

Данные о взаимодействии двух частиц хранятся в объектах класса ``Interaction``. Они содержат параметры различных потенциалов взаимодействий и моделей неупругих процессов, происходящих при столкновении частиц.
Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Interaction::Interaction(const Particle &Particle1, const Particle &Particle2, const std::string &filename = "interaction.yaml")

   Создает объект типа Interaction, загружая данные о взаимодействии двух частиц из базы данных

   **Параметры**:

       * ``const Particle &Particle1`` - первая частица, участвующая во взаимодействии

       * ``const Particle &Particle2`` - вторая частица, участвующая во взаимодействии

       * ``const std::string &filename`` - путь к файлу базы данных

Класс имеет следующие поля:

.. cpp:member:: std::string kappa::Interaction::particle1_name

   Название первой частицы, участвующей во взаимодействии

.. cpp:member:: std::string kappa::Interaction::particle2_name

   Название второй частицы, участвующей во взаимодействии

.. cpp:member:: int kappa::Interaction::charge1

   Заряд первой частицы, участвующей во взаимодействии (выраженный в элементарных электрических зарядах)

.. cpp:member:: int kappa::Interaction::charge2

   Заряд второй частицы, участвующей во взаимодействии (выраженный в элементарных электрических зарядах)

.. cpp:member:: double kappa::Interaction::collision_mass

   Приведеннная масса сталкивающихся частиц

.. cpp:member:: double kappa::Interaction::collision_diameter

   Диаметр столкновения частиц

.. cpp:member:: double kappa::Interaction::epsilon

   Глубина потенциальной ямы в потенциале Леннарда-Джонса 

.. cpp:member:: bool kappa::Interaction::vss_data

   Присутствуют ли данные потенциала VSS для данного взаимодействия (``true`` если присутствуют, ``false`` если отсутствуют)

.. cpp:member:: double kappa::Interaction::vss_dref

   Эталонный диаметр :math:`d_{ref}` в потенциале VSS

.. cpp:member:: double kappa::Interaction::vss_omega

   Параметр :math:`\omega` в потенциале VSS. Диаметр столкновения выражается через относительную скорость сталкивающихся частиц :math:`g` как 

   .. math::
     d = d_{ref} (g_{ref} / g)^(\omega - 0.5)

   где :math:`g_{ref}` - эталонная скорость

.. cpp:member:: double kappa::Interaction::vss_alpha

   Значение параметра :math:`\alpha`, описывающего рассеяние частиц, в потенциале VSS

.. cpp:member:: double kappa::Interaction::vss_Tref

   Эталонная температура :math:`T_{ref}` в потенциале VSS

.. cpp:member:: double kappa::Interaction::vss_c_d

   Вспомогательная величина :math:`vss_{c,d}`, диаметр столкновения выражается через нее по формуле 

   .. math::
     d = vss_{c,d} g^(0.5 - \omega)

.. cpp:member:: double kappa::Interaction::vss_c_cs

   Вспомогательная величина :math:`vss_{c,cs}`, сечение столкновения выражается через нее по формуле 

   .. math::
     \pi d^2 = vss_{c,cs} g^(1 - 2\omega)

.. cpp:member:: interaction_types kappa::Interaction::interaction_type

   Тип взаимодействия, возможные значения:

       * ``kappa::interaction_types::interaction_neutral_neutral`` - взаимодействие незаряженных частиц

       * ``kappa::interaction_types::interaction_neutral_ion`` - взаимодействие нейтральной частиц и иона

       * ``kappa::interaction_types::interaction_neutral_electron`` - взаимодействие нейтральной частиц и электрона

       * ``kappa::interaction_types::interaction_charged_charged`` - взаимодействие двух заряженных частиц

Класс имеет следующие методы:

.. cpp:function:: const kappa::Interaction::double& operator[](const std::string &name) const

   Доступ к скалярному параметру взаимодействия по названию параметра

.. cpp:function:: const kappa::Interaction::double& operator[](const char* name) const

   Доступ к скалярному параметру взаимодействия по названию параметра

