
# coding: utf-8

# In[1]:

import re
import textwrap


# In[2]:

def search_tag(tag, text, capture=True, greedy=True):
#     if capture:
#         mid = '([\s\S]*)'
#     else:
    mid = '[\s\S]*'
    if greedy:
        mid += '?'
    if capture:
        mid = '(' + mid +')'
    re_string = '<' + tag + '>' + mid + '<\/' + tag + '>'
    return re.search(re_string, text)


# In[3]:

def clean_whitespace(text):
    return ' '.join(text.split())


# In[4]:

def extract_cpp_names(text, output_list):
    """
    Extract function, field and method definitions
    """
    first_field = re.match('([\s\S]*?);', text)
    if first_field:
        output_list.append(clean_whitespace(first_field.group(1)))
    for m in re.finditer('<\/fdoc>([\s\S]*?);([\s\S]*?)<fdoc>', text):
        output_list.append(clean_whitespace(m.group(1)))


# In[5]:

def extract_cpp_fnames(text, output_list):
    text = re.sub('<ddoc>[\s\S]*?<\/ddoc>', '', text)
    text = re.sub('\s*=[\s\S]*?;', ';', text)
    text = text.replace('//', '')
    for m in re.finditer('([\s\S]*?);', text):
        output_list.append(clean_whitespace(m.group(1)))


# In[6]:

def process_text(tag, text, output_list):
    jj = 0
    for m in re.finditer('<' + tag + '>([\s\S]*?)<\/' + tag + '>', text):
        s = m.group(1)
        flag = True
        numins = 1
        insertions_list = []
        while flag:
            m = search_tag('i' + str(numins), s, True, False)
            if m:
                numins += 1
                insertions_list.append(m.group(1))
            else:
                flag = False
        nk = None
        for i in range(numins-1):
            nk = re.sub('<insert>i' + str(i+1) + '<\/insert>', insertions_list[i].replace("\\", "\\\\"), s)
        if nk:
            s = re.search('<doc lang="' + lang + '">([\s\S]*?)<\/doc>', nk)
        else:
            s = re.search('<doc lang="' + lang + '">([\s\S]*?)<\/doc>', s)
        if s:
            s = s.group(1).replace('\t', '    ')
            s = textwrap.dedent(s)
            if s[0:1] == '\n':
                s = s[1:]
        s = s.replace('<imath>', ':math:`')
        s = s.replace('</imath>', '`')
#         s = s.replace('</imath>', '`')
        s = s.replace('<math>', '\n\n.. math::\n  ')
        s = s.replace('</math>, ', ',\n\n')
        s = s.replace('</math>', '\n\n')
        output_list.append(s)


# In[7]:

def process_classdocs(docs_code):
    constructor_list = []
    functions_list = []
    fields_list = []
    
    constructor_docs = []
    function_docs = []
    field_docs = []

    constructor_docs_text = search_tag('constructor_docs', docs_code)
    if constructor_docs_text:
        constructor_docs_text = constructor_docs_text.group(1).replace('*/', '').replace('/*', '')
        extract_cpp_names(constructor_docs_text, constructor_list)
        process_text('cdoc', docs_code, constructor_docs)
        
    field_docs_text = search_tag('field_docs', docs_code)
    if field_docs_text:
        field_docs_text = field_docs_text.group(1).replace('*/', '').replace('/*', '')
#         print('FIELDDOCS', field_docs_text)
        extract_cpp_fnames(field_docs_text, fields_list)
#         print(fields_list)
        process_text('ddoc', docs_code, field_docs)
        
    function_docs_text = search_tag('method_docs', docs_code)
    if function_docs_text:
        function_docs_text = function_docs_text.group(1).replace('*/', '').replace('/*', '')
        extract_cpp_names(function_docs_text, functions_list)
        process_text('fdoc', docs_code, function_docs)
    
    return (constructor_list, fields_list, functions_list, constructor_docs, field_docs, function_docs)


# In[8]:

def split_type_name_func(text):
    spos = re.search(r'\s[\s\S]*?\([\s\S]*?\)', text).start()
    return [text[:spos], text[spos+1:]]


# In[9]:

def split_type_name_field(text):
    text = text.split(' ')
    return [' '.join(text[:-1]), text[-1]]


# In[11]:

filenames = ['molecule', 'approximation', 'mixture', 'particle', 'atom', 'interaction']

for filename in filenames:
    path = '../../kappa/src/include/' + filename + '.hpp'
    start_rst_path = filename + '_raw_base.rst'
    with open(path, encoding="utf-8") as f:
        raw_code = f.read()
    with open(start_rst_path, encoding="utf-8") as f:
        raw_rst_code = f.read()

    prepend = 'kappa::' + filename[0].upper() + filename[1:]

    lang = 'rus' # command-line param

    res = process_classdocs(raw_code)

    if len(res[0]) != 0:
        docs_out = 'Класс имеет следующие конструкторы:\n\n'
        for i in zip(res[0], res[3]):
            docs_out += '.. cpp:function:: ' + prepend + '::' + i[0] + '\n\n'
            tmpstr = '   ' + i[1]
            docs_out += tmpstr.replace('\n', '\n   ')
        #     splstr = i[1].split('\n')
        #     for s in splstr:
        #         docs_out += '    ' + s + '\n'
            docs_out += '\n'
        #     print(i[0], i[1])

    if len(res[1]) != 0:
        docs_out += 'Класс имеет следующие поля:\n\n'
        for i in zip(res[1], res[4]):
            tt = split_type_name_field(i[0])
            docs_out += '.. cpp:member:: ' + tt[0] + ' ' + prepend + '::' + tt[1] + '\n\n'
            tmpstr = '   ' + i[1]
            docs_out += tmpstr.replace('\n', '\n   ')
        #     splstr = i[1].split('\n')
        #     for s in splstr:
        #         docs_out += '    ' + s + '\n'
            docs_out += '\n'


    if len(res[2]) != 0:
        docs_out += 'Класс имеет следующие методы:\n\n'
        for i in zip(res[2], res[5]):
            tt = split_type_name_func(i[0])
            docs_out += '.. cpp:function:: ' + tt[0] + ' ' + prepend + '::' + tt[1] + '\n\n'
            tmpstr = '   ' + i[1]
            docs_out += tmpstr.replace('\n', '\n   ')
        #     splstr = i[1].split('\n')
        #     for s in splstr:
        #         docs_out += '    ' + s + '\n'
            docs_out += '\n'
    docs_out = re.sub(r'^\s*$', '\n', docs_out, flags=re.MULTILINE)
    docs_out = docs_out.replace('\n\n\n', '\n\n')

    with open(filename + '_raw.rst', 'w', encoding="utf-8") as f:
        f.write(raw_rst_code + docs_out)


# In[309]:




# In[310]:

# print(docs_out)


# In[311]:




# In[ ]:



