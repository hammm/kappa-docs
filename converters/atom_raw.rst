Класс атомов
------------

Данные об атомах хранятся в объектах класса ``Atom``. Он имеет те же поля, что и класс ``Particle``.
Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Atom::Atom(const std::string &name, const std::string &filename = "particles.yaml")

   Создает объект типа Atom, загружая данные об атоме из базы данных

   **Параметры**:

       * ``const std::string &name`` - название атома

       * ``const std::string &filename`` - путь к файлу базы данных

