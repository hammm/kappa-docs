Классы частиц
=============

Общий класс частиц
------------------

Данные о частицах хранятся в объектах класса ``Particle``.

Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Particle::Particle(const std::string &name, const std::string &filename = "particles.yaml")

   Создает объект типа Particle, загружая данные о частице из базы данных

   **Параметры**:

       * ``const std::string &name`` - название молекулы

       * ``const std::string &filename`` - путь к файлу базы данных

Класс имеет следующие поля:

.. cpp:member:: std::string kappa::Particle::name

   Название частицы

.. cpp:member:: double kappa::Particle::mass

   Масса частицы

.. cpp:member:: double kappa::Particle::diameter

   Диаметр частицы

.. cpp:member:: int kappa::Particle::charge

   Заряд частицы (выраженный в элементарных электрических зарядах)

.. cpp:member:: double kappa::Particle::formation_energy

   Энергия образования частицы

.. cpp:member:: double kappa::Particle::LennardJones_epsilon

   Глубина потенциальной ямы в потенциале Леннарда-Джонса (если частица является электроном, считается равным 0)

.. cpp:member:: double kappa::Particle::ionization_potential

   Потенциал ионизации частицы (если частица является электроном, считается равным 0)

.. cpp:member:: int kappa::Particle::num_electron_levels

   Число электронных уровней частицы (если частица является электроном, считается равным 0)

.. cpp:member:: arma::vec kappa::Particle::electron_energy

   Вектор энергий электронных уровней частицы (если частица является электроном, не инициализируется)

.. cpp:member:: arma::Col<unsigned long> kappa::Particle::statistical_weight

   Вектор стат. весов электронных уровней частицы (если частица является электроном, не инициализируется)

