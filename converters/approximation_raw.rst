.. _approximation-sts-label:
Поуровневое приближение
=======================

Поуровневое приближение описывается объектом класса ``Approximation``.
Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Approximation::Approximation()

   Создает объект типа Approximation

Класс имеет следующие методы:

.. cpp:function:: double kappa::Approximation::debye_length(double T, const arma::vec &concentrations, const arma::vec &charges)

   Производит расчет Дебаевской длины

   **Параметры**:

       * ``T`` - температура смеси

       * ``const arma::vec &concentrations`` - числовые плотности частиц

       * ``const arma::vec &charges`` - (размерные) заряды частиц

.. cpp:function:: int kappa::Approximation::max_electron_level(const kappa::Atom &atom, double Delta_E)

   Рассчитывает номер максимального допустимого электронного уровня :math:`e` для атома, который удовлетворяет соотношениям:

   .. math::
     \varepsilon^{el}_{e} \le \varepsilon_{ion} - \Delta E,

   .. math::
     \varepsilon^{el}_{e+1} > \varepsilon_{ion} - \Delta E

   **Параметры**:

       * ``kappa::Atom const &atom`` - рассматриваемый атом

       * ``double Delta_E`` - значение величины :math:`\Delta E` (в Джоулях)

.. cpp:function:: double kappa::Approximation::Z_rot(double T, const kappa::Molecule &molecule, int i=0, int e=0)

   Рассчитывает вращательную статистическую сумму

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``int i`` - Колебательный уровень молекулы (значение по умолчанию 0)

       * ``int e`` - Электронный уровень молекулы (значение по умолчанию 0)

.. cpp:function:: double kappa::Approximation::Z_vibr_eq(double T, const kappa::Molecule &molecule, int e=0)

   Рассчитывает равновесную колебательную статистическую сумму

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``int e`` - Электронный уровень молекулы (значение по умолчанию 0)

.. cpp:function:: double kappa::Approximation::Z_electron(double T, const kappa::Molecule &molecule, int num_electron_levels)

   Рассчитывает равновесную электронную статистическую сумму

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``int num_electron_levels`` - число учитываемых электронных уровней; если равно -1, то учитываются все электронные состояния

.. cpp:function:: double kappa::Approximation::avg_rot_energy(double T, const kappa::Molecule &molecule, int i=0, int e=0)

   Рассчитывает осредненную вращательную энергию

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``int i`` - Колебательный уровень молекулы (значение по умолчанию 0)

       * ``int e`` - Электронный уровень молекулы (значение по умолчанию 0)

.. cpp:function:: double kappa::Approximation::avg_rot_energy_sq(double T, const kappa::Molecule &molecule, int i=0, int e=0)

   Рассчитывает осредненный квадрат вращательной энергии

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``int i`` - Колебательный уровень молекулы (значение по умолчанию 0)

       * ``int e`` - Электронный уровень молекулы (значение по умолчанию 0)

.. cpp:function:: double kappa::Approximation::c_tr(double T, const kappa::Particle &particle)

   Рассчитывает удельную теплоемкость поступательных степеней свободы

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Particle &particle`` - частица, для которой производится расчет

.. cpp:function:: double kappa::Approximation::c_rot(double T, const kappa::Molecule &molecule, int i=0, int e=0)

   Рассчитывает удельную теплоемкость вращательных степеней свободы

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``int i`` - Колебательный уровень молекулы (значение по умолчанию 0)

       * ``int e`` - Электронный уровень молекулы (значение по умолчанию 0)

.. cpp:function:: double kappa::Approximation::probability_VV(double rel_vel, const kappa::Molecule &molecule1, const kappa::Molecule &molecule2, const kappa::Interaction &interaction, int i, int k, int delta_i, int e1, int e2, kappa::models_prob_vv model=kappa::models_prob_vv::model_prob_vv_fho)

   Рассчитывает вероятность VV перехода 

   .. math::
     M_{1}(e1, i) + M_{2}(e2, k) \to M_{1}(e1, i+\Delta i) + M_{2}(e2, k-\Delta i)

   **Параметры**:

       * ``double rel_vel`` - относительная скорость частиц

       * ``const kappa::Molecule &molecule1`` - молекула :math:`M_{1}`

       * ``const kappa::Molecule &molecule2`` - молекула :math:`M_{2}`

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i`` - колебательный уровень молекулы :math:`M_{1}`

       * ``int k`` - колебательный уровень молекулы :math:`M_{2}`

       * ``int delta_i`` - изменение колебательного уровня молекулы :math:`M_{1}` (величина :math:`\Delta i`)

       * ``int e1`` - электронный уровень молекулы :math:`M_{1}`

       * ``int e2`` - электронный уровень молекулы :math:`M_{2}`

       * ``kappa::models_prob_vv model`` - модель для расчета вероятности (значение по умолчанию ``kappa::models_prob_vv::model_prob_vv_fho``), возможные значения:

           * ``kappa::models_prob_vv::model_prob_vv_fho`` - модель FHO

.. cpp:function:: double kappa::Approximation::probability_VT(double rel_vel, const kappa::Molecule &molecule, const kappa::Interaction &interaction, int i, int delta_i, int e, kappa::models_prob_vt model=kappa::models_prob_vt::model_prob_vt_fho)

   Рассчитывает вероятность VT перехода 

   .. math::
     M(e, i) + P \to M(e, i+\Delta i) +P

   **Параметры**:

       * ``double rel_vel`` - относительная скорость частиц

       * ``const kappa::Molecule &molecule`` - молекула :math:`M`

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i`` - колебательный уровень молекулы :math:`M`

       * ``int delta_i`` - изменение колебательного уровня молекулы :math:`M` (величина :math:`\Delta i`)

       * ``int e`` - электронный уровень молекулы :math:`M`

       * ``kappa::models_prob_vt model`` - модель для расчета вероятности (значение по умолчанию ``kappa::models_prob_vt::model_prob_vt_fho``), возможные значения:

           * ``kappa::models_prob_vt::model_prob_vt_fho`` - модель FHO

.. cpp:function:: double kappa::Approximation::probability_diss(double rel_vel, const kappa::Molecule &molecule, const kappa::Interaction &interaction, int i, int e, kappa::models_prob_diss model=kappa::models_prob_diss::model_prob_diss_thresh_cmass_vibr)

   Рассчитывает вероятность реакции диссоциации 

   .. math::
     AB(e, i) + P \to A + B +P

   **Параметры**:

       * ``double rel_vel`` - относительная скорость частиц

       * ``const kappa::Molecule &molecule`` - молекула :math:`AB`

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i`` - колебательный уровень молекулы :math:`AB`

       * ``int e`` - электронный уровень молекулы :math:`AB`

       * ``kappa::models_prob_diss model`` - модель для расчета вероятности (значение по умолчанию ``kappa::models_prob_diss::model_prob_diss_thresh_cmass_vibr``), возможные значения:

           * ``kappa::models_prob_diss::model_prob_diss_thresh_cmass_vibr`` - пороговая модель, учитывающая колебательную энергию и поступательную энергию вдоль линии центра масс

           * ``kappa::models_prob_diss::model_prob_diss_thresh_vibr`` - пороговая модель, учитывающая колебательную энергию и полную поступательную энергию

           * ``kappa::models_prob_diss::model_prob_diss_thresh_cmass`` - пороговая модель, учитывающая поступательную энергию вдоль линии центра масс

           * ``kappa::models_prob_diss::model_prob_diss_thresh`` - пороговая модель, учитывающая полную поступательную энергию

.. cpp:function:: double kappa::Approximation::probability_diss(double rel_vel, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, kappa::models_prob_diss model=kappa::models_prob_diss::model_prob_diss_thresh_cmass_vibr)

   Рассчитывает вероятность реакции диссоциации (предполагается, что молекула :math:`AB` находится в основном электронном состоянии) (см. документацию предыдущей функции, параметр ``e=0``) 

   .. math::
     AB(i) + P \to A + B +P

.. cpp:function:: double kappa::Approximation::crosssection_elastic(double rel_vel, kappa::Interaction const &interaction, kappa::models_cs_elastic model=kappa::models_cs_elastic::model_cs_el_vss)

   Рассчитывает упругое сечение столкновения

   **Параметры**:

       * ``double rel_vel`` - относительная скорость частиц

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``kappa::models_prob_diss model`` - модель для расчета сечения (значение по умолчанию ``kappa::models_cs_elastic::model_cs_el_vss``), возможные значения:

           * ``kappa::models_cs_elastic::model_cs_el_rs`` - модель твердых сфер (RS)

           * ``kappa::models_cs_elastic::model_cs_el_vss`` - модель переменных мягких сфер (VSS)

.. cpp:function:: double kappa::Approximation::crosssection_VT(double rel_vel, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int delta_i, int e, kappa::models_cs_vt model=kappa::models_cs_vt::model_cs_vt_vss_fho)

   Расчет сечения VT перехода 

   .. math::
     M(e, i) + P \to M(e, i+\Delta i) +P

   **Параметры**:

       * ``double rel_vel`` - относительная скорость частиц

       * ``const kappa::Molecule &molecule`` - молекула для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i`` - колебательный уровень молекулы 

       * ``int delta_i - изменение колебательного уровня молекулы 

       * ``int e`` - электронный уровень молекулы

       * ``kappa::models_cs_vt model`` - модель для расчета сечения VT-перехода (значение по умолчанию ``kappa::models_cs_vt::model_cs_vt_vss_fho``), возможные значения:

           * ``kappa::models_cs_vt::model_cs_vt_rs_fho`` - модель твердых сфер (RS) в сочетании с моделью FHO 

           * ``kappa::models_cs_vt::model_cs_vt_vss_fho`` - модель сфер переменного диаметра (VSS) в сочетании с моделью FHO 

.. cpp:function:: double kappa::Approximation::crosssection_VT(double rel_vel, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int delta_i, kappa::models_cs_vt model=kappa::models_cs_vt::model_cs_vt_vss_fho)

   Расчет сечения VT перехода (предполагается, что молекула :math:`AB` находится в основном электронном состоянии) (см. документацию предыдущей функции, параметр ``e=0``) 

   .. math::
     M(i) + P \to M(i+\Delta i) +P

.. cpp:function:: double kappa::Approximation::crosssection_diss(double rel_vel, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int e, kappa::models_cs_diss model=kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass_vibr)

   Рассчитывает сечение реакции диссоциации 

   .. math::
     AB(e, i) + P \to A + B +P

   **Параметры**:

       * ``double rel_vel`` - относительная скорость частиц

       * ``const kappa::Molecule &molecule`` - молекула :math:`AB`

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i`` - колебательный уровень молекулы :math:`AB`

       * ``int e`` - электронный уровень молекулы :math:`AB`

       * ``kappa::models_cs_diss model`` - модель для расчета сечения (значение по умолчанию ``model_cs_diss_vss_thresh_cmass_vibr``), возможные значения:

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh_cmass_vibr`` - модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей колебательную энергию и поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh_vibr`` - модель твердых сфер (RS) в сочетании с пороговой модель, учитывающей колебательную энергию и полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh_cmass`` - модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh`` - модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass_vibr`` - модель переменных мягких сфер (VSS) в сочетании с пороговой моделью,
             учитывающей колебательную энергию и поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh_vibr`` - модель переменных мягких сфер (VSS) в сочетании с пороговой модель, учитывающей колебательную энергию и полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass`` - модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh`` - модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_ilt`` - модель, основанная на преобразовании Лапласа от резултьтатов квази-классических траекторных расчетов в базе данных Phys4Entry 

.. cpp:function:: double kappa::Approximation::crosssection_diss(double rel_vel, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, kappa::models_cs_diss model=kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass_vibr)

   Рассчитывает сечения реакции диссоциации (предполагается, что молекула :math:`AB` находится в основном электронном состоянии) (см. документацию предыдущей функции, параметр ``e=0``) 

   .. math::
     AB(i) + P \to A + B + P

.. cpp:function:: double kappa::Approximation::Z_coll(double T, double n, kappa::Interaction const &interaction)

   Число столкновений в единицу времени

   **Параметры**:

       * ``double T`` - температура

       * ``double n`` - числовая плотность

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

.. cpp:function:: double kappa::Approximation::rot_collision_number_parker(double T, kappa::Molecule const &molecule, kappa::Interaction const &interaction)

   Число столкновений, необходимое для установления равновесия по вращательным степеням свободы, вычисленное по формуле Паркера

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

.. cpp:function:: double kappa::Approximation::rot_relaxation_time_parker(double T, double n, kappa::Molecule const &molecule, kappa::Interaction const &interaction, kappa::models_omega model=kappa::models_omega::model_omega_esa)

   Время вращательной релаксации, вычисленное по формуле Паркера

   **Параметры**:

       * ``double T`` - температура

       * ``double n`` - числовая плотность

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``kappa::models_omega model`` - модель для вычисления Омега-интеграла (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``):

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

.. cpp:function:: double kappa::Approximation::vibr_relaxation_time_MW(double T, double n, kappa::Molecule const &molecule, kappa::Interaction const &interaction)

   Время колебательной релаксации, вычисленное по формуле Милликена-Уайта

   **Параметры**:

       * ``double T`` - температура

       * ``double n`` - числовая плотность

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

.. cpp:function:: double kappa::Approximation::vibr_relaxation_time_Park_corr(double T, double n, kappa::Interaction const &interaction, double crosssection=1e-10)

   Поправка Парка к формуле Милликена-Уайта для вычисления времени колебательной релаксации 

   **Параметры**:

       * ``double T`` - температура

       * ``double n`` - числовая плотность

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``double crosssection`` - сечение для расчета поправки Парка (значение по умолчанию ``1e-10``)

.. cpp:function:: double kappa::Approximation::omega_integral(double T, kappa::Interaction const &interaction, int l, int r, kappa::models_omega model=kappa::models_omega::model_omega_esa, bool dimensional = true)

   Вычисление Омега-интегралов для взаимодействий нейтральных частиц, взаимодействий ионов и нейтральных частиц, для взаимодействия электронов и нейтральных частиц

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int l`` - степень интеграла

       * ``int r`` - степень интеграла

       * ``bool dimensional`` - тип возвращаемого интеграла (размерный/безразмерный) (значение по умолчанию ``true``)

       * ``kappa::models_omega model`` - модель для вычисления Омега-интеграла (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``), возможные значения:

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

.. cpp:function:: double kappa::Approximation::omega_integral(double T, kappa::Interaction const &interaction, int l, int r, double debye_length, kappa::models_omega model=kappa::models_omega::model_omega_esa, bool dimensional = true)

   Вычисление Омега-интегралов для взаимодействий произвольных частиц

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int l`` - степень интеграла

       * ``int r`` - степень интеграла

       * ``double debye_length` - Дебаевская длина (используется при расчете интегралов для взаимодействий заряженных частиц)

       * ``bool dimensional`` - тип возвращаемого интеграла (размерный/безразмерный) (значение по умолчанию ``true``)

       * ``kappa::models_omega model`` - модель для вычисления Омега-интеграла (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``),
         возможные значения (для столкновений заряженных частиц данный параметр ни на что не влияет):

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

.. cpp:function:: double kappa::Approximation::integral_VT(double T, int degree, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int delta_i, int e, kappa::models_cs_vt model = kappa::models_cs_vt::model_cs_vt_vss_fho)

   Вычисление интегралов по сечению VT-перехода 

   .. math::
     M(e, i) + P \to M(e, i+\Delta i) + P

   **Параметры**:

       * ``double T`` - температура

       * ``int degree`` -  степень интеграла

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i`` - колебательный уровень молекулы

       * ``int delta_i`` - изменение колебательного уровня молекулы

       * ``int e`` -электронный уровень молекулы

       * ``kappa::models_cs_vt model`` - модель для вычисления интегралов по сечению VT-перехода (значение модели по умолчанию ``model_cs_vt_vss_fho``), возможные значения:

           * ``kappa::models_cs_vt::model_cs_vt_rs_fho`` - модель твердых сфер (RS) в сочетании с моделью FHO 

           * ``kappa::models_cs_vt::model_cs_vt_vss_fho`` - модель сфер переменного диаметра (VSS) в сочетании с моделью FHO 

.. cpp:function:: double kappa::Approximation::integral_VT(double T, int degree, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int delta_i, kappa::models_cs_vt model = kappa::models_cs_vt::model_cs_vt_vss_fho)

   Вычисление интегралов по сечению VT-перехода (предполагается, что молекула :math:`M` находится в основном электронном состоянии) (см. документацию предыдущей функции, параметр ``e=0``) 

   .. math::
     M(i) + P \to M(i+\Delta i) + P

.. cpp:function:: double kappa::Approximation::k_VT(double T, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int delta_i, int e, kappa::models_k_vt model = kappa::models_k_vt::model_k_vt_vss_fho)

   Вычисление коэффициента скорости VT-перехода 

   .. math::
     M(e, i) + P \to M(e, i+\Delta i) + P

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i`` - колебательный уровень молекулы

       * ``int delta_i`` - изменения колебательного уровня молекулы

       * ``int e`` -  электронный уровень молекулы

       * ``kappa::models_k_vt model`` - модель для расчета коэффициента скорости (значение модели по умолчанию ``model_k_vt_vss_fho``), возможные значения:

           * ``kappa::models_k_vt::model_k_vt_rs_fho`` - модель твердых сфер (RS) в сочетании с моделью FHO 

           * ``kappa::models_k_vt::model_k_vt_vss_fho`` - модель переменных мягких сфер (VSS) в сочетании с моделью FHO 

           * ``kappa::models_k_vt::model_k_vt_ssh`` - модель SSH 

           * ``kappa::models_k_vt::model_k_vt_phys4entry`` -  аппроксимация из базы данных Phys4Entry

           * ``kappa::models_k_vt::model_k_vt_billing`` - аппроксимация траекторных расчетов Биллинга 

.. cpp:function:: double kappa::Approximation::k_VT(double T, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int delta_i, kappa::models_k_vt model = kappa::models_k_vt::model_k_vt_vss_fho)

   Вычисление коэффициента скорости VT-перехода (предполагается, что молекула :math:`M` находится в основном электронном состоянии) (см. документацию предыдущей функции, параметр ``e=0``) 

   .. math::
     M(i) + P \to M(i+\Delta i) + P

.. cpp:function:: double kappa::Approximation::k_exch(double T, kappa::Molecule const &molecule, kappa::Atom const &atom, kappa::Interaction const &interaction, int i, int e, int num_electron_levels=-1, kappa::models_k_exch model=kappa::models_k_exch::model_k_exch_warnatz)

   Вычисление коэффициентов скорости обменных реакций (для молекул с электронным возбуждением) без учета колебательного и электронного возбуждения молекулы-продукта реакции

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``kappa::Atom const &atom`` - рассматриваемый атом

       * ``int i`` - колебательный уровень исходной молекулы

       * ``int k`` - колебательный уровень образующейся молекулы 

       * ``int e`` - электронный уровень молекулы

       * ``int num_electron_level`` - число учитываемых электронных уровней молекулы-реагента (значение по умолчанию =-1 - учитываются все имеющиеся уровни)

       * ``kappa::models_k_exch model`` - модель для расчета коэффициента скорости обменной реакции (значение модели по умолчанию ``model_k_exch_warnatz``), возможные значения:

           * ``kappa::models_k_exch::model_k_exch_arrh_scanlon - модель Аррениуса в сочетании с данными из Scanlon et al.

           * ``kappa::models_k_exch::model_k_exch_arrh_park`` - модель Аррениуса в сочетании с данными из Park et al.

           * ``kappa::models_k_exch::model_k_exch_warnatz`` - модель Варнатца

           * ``kappa::models_k_exch::model_k_exch_rf`` - модель Русанова-Фридмана

           * ``kappa::models_k_exch::model_k_exch_polak`` - модель Полака

.. cpp:function:: double kappa::Approximation::k_exch(double T, kappa::Molecule const &molecule, kappa::Atom const &atom, kappa::Interaction const &interaction, int i, kappa::models_k_exch model=kappa::models_k_exch::model_k_exch_warnatz)

   Вычисление коэффициентов скорости обменных реакций (для молекул без электронного возбуждения) без учета колебательного и электронного возбуждения молекулы-продукта реакции
   (см. документацию предыдущей функции, параметр ``e=0``)

.. cpp:function:: double kappa::Approximation::k_exch(double T, kappa::Molecule const &molecule, kappa::Atom const &atom, kappa::Molecule const &molecule_prod, kappa::Interaction const &interaction, int i, int k, int e, int num_electron_levels=-1, kappa::models_k_exch model=kappa::models_k_exch::model_k_exch_maliat_infty_arrh_scanlon)

   Вычисление коэффициентов скорости обменных реакций с учетом колебательного возбуждения молекулы-продукта реакции (для молекул с электронным возбуждением). В случае, если используется не модифицированная модель Алиата,
   колебательное возбуждение продукта реакции учитываться не будет.

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``kappa::Atom const &atom`` - рассматриваемый атом

       * ``int i`` - колебательный уровень исходной молекулы

       * ``int k`` - колебательный уровень образующейся молекулы 

       * ``int e`` - электронный уровень молекулы

       * ``int num_electron_levels`` - число учитываемых электронных уровней молекулы-реагента (значение по умолчанию =-1 - учитываются все имеющиеся уровни)

       * ``kappa::models_k_exch model`` - модель для расчета коэффициента скорости обменной реакции (значение модели по умолчанию ``model_k_exch_maliat_infty_arrh_scanlon``), возможные значения:

           * ``kappa::models_k_exch::model_k_exch_arrh_scanlon - модель Аррениуса с данными из Scanlon et al. (не учитывает электронное возбуждение молекулы-реагента и колебательное возбуждение молекулы-продукта)

           * ``kappa::models_k_exch::model_k_exch_arrh_park`` - модель Аррениуса с данными из Park et al. (не учитывает электронное возбуждение молекулы-реагента и колебательное возбуждение молекулы-продукта)

           * ``kappa::models_k_exch::model_k_exch_warnatz`` - модель Варнатца (не учитывает электронное возбуждение молекулы-реагента и колебательное возбуждение молекулы-продукта)

           * ``kappa::models_k_exch::model_k_exch_rf`` - модель Русанова-Фридмана (не учитывает электронное возбуждение молекулы-реагента и колебательное возбуждение молекулы-продукта)

           * ``kappa::models_k_exch::model_k_exch_polak`` - модель Полака (не учитывает электронное возбуждение молекулы-реагента и колебательное возбуждение молекулы-продукта)

           * ``model_k_exch_maliat_D6k_arrh_scanlon`` - модифицированная модель Алиэта с данными из Scanlon et al., :math:`U=D/6k`

           * ``model_k_exch_maliat_3T_arrh_scanlon`` - модифицированная модель Алиэта с данными из Scanlon et al., :math:`U=3Tk`

           * ``model_k_exch_maliat_infty_arrh_scanlon`` - модифицированная модель Алиэта с данными из Scanlon et al., :math:`U=\infty`

           * ``model_k_exch_maliat_D6k_arrh_park`` - модифицированная модель Алиэта с данными из Park et al., :math:`U=D/6k`

           * ``model_k_exch_maliat_3T_arrh_park`` - модифицированная модель Алиэта с данными из Park et al., :math:`U=3Tk`

           * ``model_k_exch_maliat_infty_arrh_park`` - модифицированная модель Алиэта с данными из Park et al., :math:`U=\infty`

.. cpp:function:: double kappa::Approximation::k_exch(double T, kappa::Molecule const &molecule, kappa::Atom const &atom, kappa::Molecule const &molecule_prod, kappa::Interaction const &interaction, int i, int k, kappa::models_k_exch model=kappa::models_k_exch::model_k_exch_maliat_infty_arrh_scanlon)

   Вычисление коэффициентов скорости обменных реакций с учетом колебательного возбуждения молекулы-продукта реакции (для молекул без электронного возбуждения) (см. документацию предыдущей функции,
   параметры ``e=0``, ``num_electron_levels=1``)

.. cpp:function:: double kappa::Approximation::integral_diss(double T, int degree, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int e, kappa::models_cs_diss model=kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass_vibr)

   Расчета интеграла по сечению реакции диссоциации 

   .. math::
     AB(e, i) + P \to A + B + P

   **Параметры**:

       * ``double T`` - температура

       * ``int degree`` - степень интеграла

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся молекул

       * ``int i`` - колебательный уровень молекулы

       * ``int e`` - электронный уровень молекулы

       * ``kappa::models_cs_diss model`` - модель сечения (значение по умолчанию = ``kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass_vibr``), возможные значение:

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh_cmass_vibr``- модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей колебательную энергию и поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh_vibr`` - модель твердых сфер (RS) в сочетании с пороговой модель, учитывающей колебательную энергию и полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh_cmass`` - модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_rs_thresh`` -  модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass_vibr`` -  модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей колебательную энергию
             и поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh_vibr`` -  модель переменных мягких сфер (VSS) в сочетании с пороговой модель, учитывающей колебательную энергию и полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass`` -  модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей поступательную энергию вдоль линии центра масс

           * ``kappa::models_cs_diss::model_cs_diss_vss_thresh`` - модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей полную поступательную энергию

           * ``kappa::models_cs_diss::model_cs_diss_ilt`` -  модель, основанная на обратном преобразовании Лапласа от результатов квази-классических траекторных расчетов в базе данных Phys4Entry

.. cpp:function:: double kappa::Approximation::integral_diss(double T, int degree, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, kappa::models_cs_diss model=kappa::models_cs_diss::model_cs_diss_vss_thresh_cmass_vibr)

   Расчет интеграла по сечению реакции диссоциации (предполагается, что молекула :math:`AB` находится в основном электронном состоянии) (см. документацию предыдущей функции, параметр ``e=0``) 

   .. math::
     AB(i) + P \to A + B + P

.. cpp:function:: double kappa::Approximation::k_diss(double T, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, int e, int num_electron_levels=-1, kappa::models_k_diss model=kappa::models_k_diss::model_k_diss_tm_3T_arrh_scanlon)

   Вычисление коэффициента скорости диссоциации 

   .. math::
     AB(e, i) + P \to A + B + P

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчет

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся молекул

       * ``int i`` - колебательный уровень молекулы

       * ``int e`` - электронный уровень молекулы

       * ``int num_electron_levels`` - число учитываемых электронных уровней (значение по умолчанию =-1 - учитываются все имеющиеся уровни)

       * ``kappa::models_k_diss model`` - модель для вычисление коэффициента скорости диссоциации (значение по умолчанию = ``model_k_diss_tm_3T_arrh_scanlon``), возможные значения:

           * ``kappa::models_k_diss::model_k_diss_rs_thresh_cmass_vibr``- модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей колебательную энергию и поступательную энергию вдоль линии центра масс

           * ``kappa::models_k_diss::model_k_diss_rs_thresh_vibr`` - модель твердых сфер (RS) в сочетании с пороговой модель, учитывающей колебательную энергию и полную поступательную энергию

           * ``kappa::models_k_diss::model_k_diss_rs_thresh_cmass`` - модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей поступательную энергию вдоль линии центра масс

           * ``kappa::models_k_diss::model_k_diss_rs_thresh`` - модель твердых сфер (RS) в сочетании с пороговой моделью, учитывающей полную поступательную энергию

           * ``kappa::models_k_diss::model_k_diss_vss_thresh_cmass_vibr`` - модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей колебательную энергию и поступательную энергию
             вдоль линии центра масс

           * ``kappa::models_k_diss::model_k_diss_vss_thresh_vibr`` - модель переменных мягких сфер (VSS) в сочетании с пороговой модель, учитывающей колебательную энергию и полную поступательную энергию

           * ``kappa::models_k_diss::model_k_diss_vss_thresh_cmass`` - модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей поступательную энергию вдоль линии центра масс

           * ``kappa::models_k_diss::model_k_diss_vss_thresh`` - модель переменных мягких сфер (VSS) в сочетании с пороговой моделью, учитывающей полную поступательную энергию

           * ``kappa::models_k_diss::model_k_diss_arrh_scanlon`` - модель Аррениуса с данными из Scanlon et al.

           * ``kappa::models_k_diss::model_k_diss_arrh_park`` - модель Аррениуса с данными из Park et al.

           * ``kappa::models_k_diss::model_k_diss_tm_D6k_arrh_scanlon`` - модель Тринора-Маррона с данными из Scanlon et al., :math:`U=D/6k`

           * ``kappa::models_k_diss::model_k_diss_tm_3T_arrh_scanlon`` - модель Тринора-Маррона с данными из Scanlon et al., :math:`U=3T`

           * ``kappa::models_k_diss::model_k_diss_tm_infty_arrh_scanlon`` - модель Тринора-Маррона с данными из Scanlon et al., :math:`U=\infty`

           * ``kappa::models_k_diss::model_k_diss_tm_D6k_arrh_park`` - модель Тринора-Маррона с данными из Park et al., :math:`U=D/6k`

           * ``kappa::models_k_diss::model_k_diss_tm_3T_arrh_park`` - модель Тринора-Маррона с данными из Park et al., :math:`U=3T`

           * ``kappa::models_k_diss::model_k_diss_tm_infty_arrh_park`` - модель Тринора-Маррона с данными из Park et al., :math:`U=\infty`

           * ``kappa::models_k_diss::model_k_diss_phys4entry`` - аппроксимация из базы данных Phys4Entry

           * ``kappa::models_k_diss::model_k_diss_ilt``- модель, основанная на применении обратного преобразовании Лапласа к результатам квази-классических траекторных расчетов в базе данных Phys4Entry

.. cpp:function:: double kappa::Approximation::k_diss(double T, kappa::Molecule const &molecule, kappa::Interaction const &interaction, int i, kappa::models_k_diss model=kappa::models_k_diss::model_k_diss_tm_3T_arrh_scanlon)

   Вычисление коэффициента скорости диссоциации (предполагается, что молекула :math:`AB` находится в основном электронном состоянии)
   (см. документацию предыдущей функции, параметры ``e=0``, ``num_electron_levels=1``) 

   .. math::
     AB(i) + P \to A + B + P

.. cpp:function:: arma::vec kappa::Approximation::Boltzmann_distribution(double T, double n, kappa::Molecule const &molecule, int e=0)

   Возвращает вектор заселенности колебательных уровней в соответствии с больцмановским распределением

   **Параметры**:

       * ``double T`` - температура

       * ``double n`` - числовая плотность

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчёт

       * ``int e`` - электронный уровень молекулы (значение по умолчанию = 0)

.. cpp:function:: double kappa::Approximation::k_bf_VT(double T, kappa::Molecule const &molecule, int i, int delta_i, int e=0)

   Отношение обратного и прямого коэффициентов скорости VT-перехода

   **Параметры**:

       * ``double T`` - температура

       * ``kappa::Molecule const &molecule`` - молекула, для которой производится расчёт

       * ``int i`` - колебательный уровень молекулы 

       * ``int delta_i`` - изменение колебательного уровня молекулы

       * ``int e`` - электронный уровень молекулы (значение по умолчанию = 0)

.. cpp:function:: double kappa::Approximation::k_bf_VV(double T, kappa::Molecule const &molecule1, kappa::Molecule const &molecule2, int i, int k, int delta_i, int e1=0, int e2=0)

   Отношение прямого и обратного коэффициентов скорости VV-перехода

   **Параметры**:

       * ``double T`` - температура                      

       * ``const kappa::Molecule &molecule1`` - молекула, для которой производится расчёт, в начальном состоянии 

       * ``const kappa::Molecule &molecule2`` - молекула, для которой производится расчет, в конечном состоянии

       * ``int i`` - колебательный уровень молекулы в начальном состоянии

       * ``int k`` - колебательный уровень молекулы в конечном состоянии

       * ``int delta_i`` - изменение колебательного уровня молекулы 

       * ``int e1`` - электронный уровень молекулы в начальном состоянии (значение по умолчанию = 0)

       * ``int e2`` - электронный уровень второй в конечном состоянии (значение по умолчанию = 0)

.. cpp:function:: double kappa::Approximation::k_bf_exch(double T, kappa::Molecule const &molecule_before, kappa::Atom const &atom_before, kappa::Molecule const &molecule_after, kappa::Atom const &atom_after, kappa::Interaction const &interaction, int i_before, int i_after, int e_before=0, int e_after=0)

   Отношение прямого и обратного коэффициентов скорости обменной реакции

   **Параметры**:

       * ``double T`` - температура                      

       * ``const kappa::Molecule &molecule_before`` - молекула, для которой производится расчёт, в начальном состоянии 

       * ``const kappa::Molecule &molecule_after`` - молекула, для которой производится расчет, в конечном состоянии

       * ``kappa::Atom const &atom_before`` - рассматриваемый атом в начальном состоянии

       * ``kappa::Atom const &atom_after`` - рассматриваемый атом в конечном состоянии

       * ``const kappa::Interaction &interaction`` - данные о взаимодействии сталкивающихся частиц

       * ``int i_before`` - колебательный уровень молекулы в начальном состоянии

       * ``int i_after`` - колебательный уровень молекулы в конечном состоянии

       * ``int e_before`` - электронный уровень молекулы в начальном состоянии (значение по умолчанию = 0)

       * ``int e_after`` - электронный уровень молекулы в конечном состоянии (значение по умолчанию = 0)

.. cpp:function:: double kappa::Approximation::k_bf_diss(double T, kappa::Molecule const &molecule, kappa::Atom const &atom1, kappa::Atom const &atom2, int i, int e=0)

   Отношение прямого и обратного коэффициентов скорости диссоциации

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчёт

       * ``kappa::Atom const &atom1`` - рассматриваемый атом в начальном состоянии

       * ``kappa::Atom const &atom2`` - рассматриваемый атом в конечном состоянии

       * ``int i`` - колебательный уровень молекулы

       * ``int e`` - электронный уровень молекулы в начальном состоянии (значение по умолчанию = 0)

.. cpp:function:: double kappa::Approximation::k_bf_diss(double T, kappa::Molecule const &molecule, kappa::Atom const &atom, int i, int e=0)

   Отношение прямого и обратного коэффициентов скорости диссоциации

   **Параметры**:

       * ``double T`` - температура

       * ``const kappa::Molecule &molecule`` - молекула, для которой производится расчёт

       * ``kappa::Atom const &atom`` - рассматриваемый атом

       * ``int i`` - колебательный уровень молекулы

       * ``int e`` - электронный уровень молекулы (значение по умолчанию = 0)

