.. KAPPA documentation master file, created by
   sphinx-quickstart on Mon Mar  6 12:05:01 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

KAPPA
=====

KAPPA (Kinetic Approach to modeling of re-entry Processes in Planetary Atmospheres) – библиотека на C++ для расчета свойств неравновесных течений газов с внутренними степенями свободы.
Она позволяет использовать различные уровни описания (поуровневое, многотемпературное и однотемпературное приближение) смеси.

Библиотека содержит функции расчета скорости физико-химической релаксации (учитываются процессы релаксации колебательных и электронных степеней свободы, химические реакции и ионизация), теплоемкостей, транспортных коэффициентов (теплопроводность, диффузия, сдвиговая и объемная вязкости), времен релаксации, а также различных вспомогательных величин.

Содержание:

.. toctree::
   :maxdepth: 2

   introduction
   exceptions
   particles
   interaction
   approximations
   mixture




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

