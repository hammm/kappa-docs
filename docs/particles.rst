Классы частиц
=============

Общий класс частиц
------------------

Данные о частицах хранятся в объектах класса ``Particle``.

Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Particle::Particle(const std::string &name, const std::string &filename = "particles.yaml")

   Создает объект типа Particle, загружая данные о частице из базы данных

   **Параметры**:

       * ``const std::string &name`` - название молекулы

       * ``const std::string &filename`` - путь к файлу базы данных

Класс имеет следующие поля:

.. cpp:member:: std::string kappa::Particle::name

   Название частицы

.. cpp:member:: double kappa::Particle::mass

   Масса частицы

.. cpp:member:: double kappa::Particle::diameter

   Диаметр частицы

.. cpp:member:: int kappa::Particle::charge

   Заряд частицы (выраженный в элементарных электрических зарядах)

.. cpp:member:: double kappa::Particle::formation_energy

   Энергия образования частицы

.. cpp:member:: double kappa::Particle::LennardJones_epsilon

   Глубина потенциальной ямы в потенциале Леннарда-Джонса (если частица является электроном, считается равным 0)

.. cpp:member:: double kappa::Particle::ionization_potential

   Потенциал ионизации частицы (если частица является электроном, считается равным 0)

.. cpp:member:: int kappa::Particle::num_electron_levels

   Число электронных уровней частицы (если частица является электроном, считается равным 0)

.. cpp:member:: arma::vec kappa::Particle::electron_energy

   Вектор энергий электронных уровней частицы (если частица является электроном, не инициализируется)

.. cpp:member:: arma::Col<unsigned long> kappa::Particle::statistical_weight

   Вектор стат. весов электронных уровней частицы (если частица является электроном, не инициализируется)

Класс атомов
------------

Данные об атомах хранятся в объектах класса ``Atom``. Он имеет те же поля, что и класс ``Particle``.
Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Atom::Atom(const std::string &name, const std::string &filename = "particles.yaml")

   Создает объект типа Atom, загружая данные об атоме из базы данных

   **Параметры**:

       * ``const std::string &name`` - название атома

       * ``const std::string &filename`` - путь к файлу базы данных

Класс молекул
-------------

Данные о молекулах хранятся в объектах класса ``Molecule``.
Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Molecule::Molecule(const std::string &name, bool anharmonic_spectrum=true, bool rigid_rotator = true, const std::string &filename = "particles.yaml")

   Создает объект типа Molecule, загружая данные о двухатомной молекуле из базы данных и производя расчет вращательных и колебательных спектров для каждого электронного состояния

   **Параметры**:

       * ``const std::string &name`` - название молекулы

       * ``bool anharmonic_spectrum`` - имеет ли молекула ангармонический колебательный спектр (значение по умолчанию ``true``)

       * ``bool rigid_rotator`` - является ли молекула жестким ротатором (значение по умолчанию ``true``)

       * ``const std::string &filename`` - путь к файлу базы данных

Класс имеет следующие поля:

.. cpp:member:: bool kappa::Molecule::anharmonic_spectrum

   Является ли спектр ангармоническим

.. cpp:member:: bool kappa::Molecule::rigid_rotator

   Является ли молекула жестким ротатором

.. cpp:member:: double kappa::Molecule::reduced_osc_mass

   Приведенная масса осциллятора, равная :math:`m_{A}m_{B} / (m_{A} + m_{B})`, где :math:`m_{A}`, :math:`m_{B}` - массы атомов молекулы

.. cpp:member:: double mA_mAB double mB_mAB double kappa::Molecule::rot_inertia

   Вращательный момент инерции молекулы

.. cpp:member:: double kappa::Molecule::internuclear_distance

   Межъядерное расстояние

.. cpp:member:: int kappa::Molecule::rot_symmetry

   Фактор симметрии молекул (равен 2 для гомоядерных и 1 для гетероядерных молекул)

.. cpp:member:: arma::vec kappa::Molecule::vibr_frequency

   Вектор колебательных частот (для каждого электронного состояния)

.. cpp:member:: arma::vec kappa::Molecule::vibr_we_xe

   Вектор параметра ангармоничности :math:`\omega_{e}x_{e}` (для каждого электронного состояния)

.. cpp:member:: arma::vec kappa::Molecule::vibr_we_ye

   Вектор параметра ангармоничности :math:`\omega_{e}y_{e}` (для каждого электронного состояния)

.. cpp:member:: arma::vec kappa::Molecule::vibr_we_ze

   Вектор параметра ангармоничности :math:`\omega_{e}z_{e}` (для каждого электронного состояния)

.. cpp:member:: arma::vec kappa::Molecule::rot_be

   Вектор параметра :math:`B_{e}`, описывающего зависимость вращательной энергии от вращательного уровня (для каждого электронного состояния)

.. cpp:member:: arma::vec kappa::Molecule::rot_ae

   Вектор параметра :math:`\alpha_{e}`, описывающего зависимость вращательной энергии от вращательного и колебательного уровня (для каждого электронного состояния)

.. cpp:member:: arma::vec kappa::Molecule::characteristic_vibr_temperatures

   Вектор характеристических колебательных температур (для каждого электронного состояния)

.. cpp:member:: arma::vec kappa::Molecule::diss_energy

   Вектор энергий диссоциации (для каждого электронного состояния)

.. cpp:member:: std::vector<std::vector<int> > kappa::Molecule::num_rot_levels

   Массив числа вращательных уровней (1й индекс - номер электронного уровня, 2й индекс - номер колебательного уровня; в случае, если молекула является жестким ротатором, для любого фиксированного 1го индекса
   все значения одинаков (число вращательных уровней не зависит от колебательного состояния молекулы))

.. cpp:member:: std::vector<int> kappa::Molecule::num_vibr_levels

   Массив числа колебательных уровней (для каждого электронного состояния)

.. cpp:member:: std::vector<std::vector<arma::vec> > kappa::Molecule::rot_energy

   Массив векторов энергий вращательных уровней (1й индекс - номер электронного уровня, 2й индекс - номер колебательного уровня; в случае, если молекула является жестким ротатором, для любого фиксированного 1го индекса
   все значения одинаковы (вращательный спектр не зависит от колебательного состояния молекулы))

.. cpp:member:: std::vector<arma::vec> kappa::Molecule::vibr_energy

   Вектор векторов энергий колебательных уровней (индекс - номер электронного уровня)

.. cpp:member:: double kappa::Molecule::parker_const

   Значения постоянной в формуле Паркера, используемой для расчета времен вращательной релаксации

