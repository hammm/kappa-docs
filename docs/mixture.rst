.. _mixture-sts-label:
Смесь в поуровневом приближении
===============================

Смесь в поуровневом приближении, состоящая из произвольного числа молекул и атомов, описывается объектом класса ``Mixture`` (наследует от класса ``Approximation``).
Объект класса ``Mixture`` хранит информацию о всех взаимодействиях частиц смеси, позволяет рассчитывать удельные теплоемкости, числовые и массовые плотности, транспортные коэффиценты.

**Примечание**: так как объекты класса ``Mixture`` хранят информацию о матрицах, используемых для расчета коэффициентов переноса (для оптимизации скорости расчетов), они не являются thread-safe.
Класс имеет следующие конструкторы:

.. cpp:function:: kappa::Mixture::Mixture(const std::vector<kappa::Molecule> &i_molecules, const std::vector<kappa::Atom> &i_atoms, const std::string &interactions_filename="interaction.yaml", const std::string &particles_filename="particles.yaml")

   Создает объект класса Mixture с указанными молекулами и атомами. В случае, если среди молекул и атомов есть заряженные частицы, считается, что смесь также содержит электроны.

   **Параметры**:

       * ``const std::vector<kappa::Molecule> &i_molecules`` - Вектор молекул, которые будут в смеси

       * ``const std::vector<kappa::Atom> &i_atoms`` - Вектор атомов, которые будут в смеси

       * ``const std::string &interactions_filename`` - путь к файлу базы данных взаимодействий (значение по умолчанию ``"interaction.yaml"``)

       * ``const std::string &particles_filename`` - путь к файлу базы данных частиц, необходимо для загрузки данных об электроне (значение по умолчанию ``"particles.yaml"``)

Класс имеет следующие методы:

.. cpp:function:: std::string kappa::Mixture::get_names()

   Возвращает строку с названиями частиц в смеси

.. cpp:function:: int kappa::Mixture::get_n_particles()

   Возвращает число частиц в смеси

.. cpp:function:: int kappa::Mixture::get_n_vibr_levels()

   Возвращает суммарное число колебательных уровней в смеси

.. cpp:function:: std::vector<int> kappa::Mixture::get_n_vibr_levels_array()

   Возвращает массив числа колебательных уровней молекул в смеси

.. cpp:function:: arma::vec kappa::Mixture::convert_molar_frac_to_mass(const arma::vec &x)

   Переводит массив молярных долей в массив массовых долей

   **Параметры**:

       * ``const arma::vec &x`` - массив молярных долей компонент смеси

.. cpp:function:: arma::vec kappa::Mixture::convert_mass_frac_to_molar(const arma::vec &y)

   Переводит массив массовых долей в массив молярных долей

   **Параметры**:

       * ``const arma::vec &y`` - массив массовых долей компонент смеси

.. cpp:function:: kappa::Molecule kappa::Mixture::molecule(const std::string &name)

   Возвращает объект, соответствующей молекуле, имеющейся в смеси

   **Параметры**:

       * ``const std::string &name`` - название молекулы

.. cpp:function:: kappa::Atom kappa::Mixture::atom(const std::string &name)

   Возвращает объект, соответствующей атому, имеющемуся в смеси

   **Параметры**:

       * ``const std::string &name`` - название молекулы

.. cpp:function:: kappa::Interaction kappa::Mixture::interaction(const kappa::Molecule &molecule1, const kappa::Molecule &molecule2)

   Возвращает объект взаимодействия двух молекул, имеющихся в смеси

   **Параметры**:

       * ``const kappa::Molecule &molecule1`` - первая молекула

       * ``const kappa::Molecule &molecule2`` - вторая молекула

.. cpp:function:: kappa::Interaction kappa::Mixture::interaction(const kappa::Molecule &molecule, const kappa::Atom &atom)

   Возвращает объект взаимодействия молекулы и атома, имеющихся в смеси

   **Параметры**:

       * ``const kappa::Molecule &molecule`` - молекула

       * ``const kappa::Atom &atom`` - атом

.. cpp:function:: kappa::Interaction kappa::Mixture::interaction(const kappa::Atom &atom, const kappa::Molecule &molecule)

   Возвращает объект взаимодействия молекулы и атома, имеющихся в смеси

   **Параметры**:

       * ``const kappa::Atom &atom`` - атом

       * ``const kappa::Molecule &molecule`` - молекула

.. cpp:function:: kappa::Interaction kappa::Mixture::interaction(const kappa::Atom &atom1, const kappa::Atom &atom2)

   Возвращает объект взаимодействия двух атомов, имеющихся в смеси

   **Параметры**:

       * ``const kappa::Molecule &molecule1`` - первая молекула

       * ``const kappa::Molecule &molecule2`` - вторая молекула

.. cpp:function:: double kappa::Mixture::debye_length(double T, const std::vector<arma::vec> &n_vl_molecule, const arma::vec &n_atom, double n_electrons)

   Расчет Дебаевской длины

    **Параметры**:

        * ``double T`` - температура

        * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

        * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

        * ``double n_electrons`` - числовая плотность электронов

.. cpp:function:: double kappa::Mixture::debye_length(double T, const std::vector<arma::vec> &n_vl_molecule, double n_electrons)

   Расчет Дебаевской длины (в смеси без атомов)

    **Параметры**:

        * ``double T`` - температура

        * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

        * ``double n_electrons`` - числовая плотность электронов

.. cpp:function:: double kappa::Mixture::debye_length(double T, const arma::vec &n_molecule, const arma::vec &n_atom, double n_electrons)

   Расчет Дебаевской длины

    **Параметры**:

        * ``double T`` - температура

        * ``const arma::vec &n_molecule`` - Вектор числовых плотностей молекул

        * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

        * ``double n_electrons`` - числовая плотность электронов

.. cpp:function:: double kappa::Mixture::debye_length(double T, const arma::vec &n)

   Расчет Дебаевской длины

    **Параметры**:

        * ``const arma::vec &n`` - Вектор числовых плотностей (сначала в векторе идут плотности молекул, затем атомов, затем электронов)
          (сначала в векторе идут плотности молекул, затем атомов, затем электронов, порядок молекул и атомов должен быть таким же, что использовался и при создании смеси)

.. cpp:function:: double kappa::Mixture::compute_n(const std::vector<arma::vec> &n_vl_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление числовой плотности смеси

   **Параметры**:

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::compute_n(const arma::vec &n_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление числовой плотности смеси

   **Параметры**:

       * ``const arma::vec &n_molecule`` - Вектор числовых плотностей молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::compute_n(const std::vector<arma::vec> &n_vl_molecule, double n_electrons=0.0)

   Вычисление числовой плотности смеси

   **Параметры**:

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::compute_n(const arma::vec &n)

   Вычисление числовой плотности смеси

   **Параметры**:

       * ``const arma::vec &n`` - Вектор числовых плотностей (сначала в векторе идут плотности молекул, затем атомов, затем электронов)
         (сначала в векторе идут плотности молекул, затем атомов, затем электронов, порядок молекул и атомов должен быть таким же, что использовался и при создании смеси)

.. cpp:function:: arma::vec kappa::Mixture::compute_n_molecule(const std::vector<arma::vec> &n_vl_molecule)

   Вычисление числовой плотности молекул 

   **Параметры**:

       * ``const std::vector<arma::vec> &n_vl_molecule`` -  Массив заселенностей колебательных уровней молекул

.. cpp:function:: double kappa::Mixture::compute_density(const std::vector<arma::vec> &n_vl_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление плотности смеси

   **Параметры**:

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::compute_density(const std::vector<arma::vec> &n_vl_molecule, double n_electrons=0.0)

   Вычисление плотности смеси

   **Параметры**:

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::compute_density(const arma::vec &n_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление плотности смеси

   **Параметры**:

       * ``const arma::vec &n_molecule`` - Вектор числовых плотностей молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::compute_density(const arma::vec &n)

   Вычисление плотности смеси

   **Параметры**:

       * ``const arma::vec &n`` - Вектор числовых плотностей (сначала в векторе идут плотности молекул, затем атомов, затем электронов)
         (сначала в векторе идут плотности молекул, затем атомов, затем электронов, порядок молекул и атомов должен быть таким же, что использовался и при создании смеси)

.. cpp:function:: double kappa::Mixture::c_tr(const std::vector<arma::vec> &n_vl_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление удельной теплоемкости поступательных степеней свободы 

   **Параметры**:

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::c_tr(const std::vector<arma::vec> &n_vl_molecule, double n_electrons=0.0)

   Вычисление удельной теплоемкости поступательных степеней свободы 

   **Параметры**:

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::c_tr(const arma::vec &n_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление удельной теплоемкости поступательных степеней свободы 

   **Параметры**:

       * ``const arma::vec &n_molecule`` - Вектор числовых плотностей молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::c_tr(const arma::vec &n)

   Вычисление удельной теплоемкости поступательных степеней свободы 

   **Параметры**:

       * ``const arma::vec &n`` - Вектор числовых плотностей частиц смеси
         (сначала в векторе идут плотности молекул, затем атомов, затем электронов, порядок молекул и атомов должен быть таким же, что использовался и при создании смеси)

.. cpp:function:: double kappa::Mixture::c_rot(double T, const std::vector<arma::vec> &n_vl_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление удельной теплоемкости вращательных степеней свободы 

   **Параметры**:

       * ``double T`` - Температура

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::c_rot(double T, const std::vector<arma::vec> &n_vl_molecule, double n_electrons=0.0)

   Вычисление удельной теплоемкости вращательных степеней свободы 

   **Параметры**:

       * ``double T`` - Температура

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::c_rot(double T, const arma::vec &n_molecule, const arma::vec &n_atom, double n_electrons=0.0)

   Вычисление удельной теплоемкости вращательных степеней свободы 

   **Параметры**:

       * ``double T`` - Температура

       * ``const arma::vec &n_molecule`` - Вектор числовых плотностей молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов (значение по умолчанию - 0)

.. cpp:function:: double kappa::Mixture::c_rot(double T, const arma::vec &n)

   Вычисление удельной теплоемкости вращательных степеней свободы 

   **Параметры**:

       * ``double T`` - Температура

       * ``const arma::vec &n`` - Вектор числовых плотностей частиц смеси (сначала в векторе идут плотности молекул, затем атомов, затем электронов)
         (сначала в векторе идут плотности молекул, затем атомов, затем электронов, порядок молекул и атомов должен быть таким же, что использовался и при создании смеси)

.. cpp:function:: void kappa::Mixture::compute_transport_coefficients(double T, const std::vector<arma::vec> &n_vl_molecule, const arma::vec &n_atom, double n_electrons, kappa::models_omega model=kappa::models_omega::model_omega_esa, double perturbation=1e-9)

   Вычисление коэффициентов переноса

   **Параметры**:

       * ``double T`` - Температура

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``double n_electrons`` - числовая плотность электронов

       * ``kappa::models_omega model`` - модель для расчета Омега-интегралов (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``):

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

       * ``double perturbation`` - малое возмущение, используемое для избежания вырождения систем при малых (исчезающих) концентрациях компонент (значение по умолчанию - 1e-9)

.. cpp:function:: void kappa::Mixture::compute_transport_coefficients(double T, const std::vector<arma::vec> &n_vl_molecule, const arma::vec &n_atom, kappa::models_omega model=kappa::models_omega::model_omega_esa, double perturbation=1e-9)

   Вычисление коэффициентов переноса в смеси без электронов

   **Параметры**:

       * ``double T`` - Температура

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``const arma::vec &n_atom`` - Вектор числовых плотностей атомов

       * ``kappa::models_omega model`` - модель для расчета Омега-интегралов (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``):

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

       * ``double perturbation`` - малое возмущение, используемое для избежания вырождения систем при малых (исчезающих) концентрациях компонент (значение по умолчанию - 1e-9)

.. cpp:function:: void kappa::Mixture::compute_transport_coefficients(double T, const std::vector<arma::vec> &n_vl_molecule, double n_electrons, kappa::models_omega model=kappa::models_omega::model_omega_esa, double perturbation=1e-9)

   Вычисление коэффициентов переноса в смеси без атомов

   **Параметры**:

       * ``double T`` - Температура

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``double n_electrons`` - числовая плотность электронов

       * ``kappa::models_omega model`` - модель для расчета Омега-интегралов (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``):

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

       * ``double perturbation`` - малое возмущение, используемое для избежания вырождения систем при малых (исчезающих) концентрациях компонент (значение по умолчанию - 1e-9)

.. cpp:function:: void kappa::Mixture::compute_transport_coefficients(double T, const std::vector<arma::vec> &n_vl_molecule, kappa::models_omega model=kappa::models_omega::model_omega_esa, double perturbation=1e-9)

   Вычисление коэффициентов переноса в смеси без атомов и электронов

   **Параметры**:

       * ``double T`` - Температура

       * ``const std::vector<arma::vec> &n_vl_molecule`` - Массив заселенностей колебательных уровней молекул

       * ``kappa::models_omega model`` - модель для расчета Омега-интегралов (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``):

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

       * ``double perturbation`` - малое возмущение, используемое для избежания вырождения систем при малых (исчезающих) концентрациях компонент (значение по умолчанию - 1e-9)

.. cpp:function:: void kappa::Mixture::compute_transport_coefficients(double T, const arma::vec &n, kappa::models_omega model=kappa::models_omega::model_omega_esa, double perturbation=1e-9)

   Вычисление коэффициентов переноса в смеси

   **Параметры**:

       * ``double T`` - Температура

       * ``const arma::vec &n`` - Вектор числовых плотностей частиц смеси
         (сначала в векторе идут плотности молекул, затем атомов, затем электронов, порядок молекул и атомов должен быть таким же, что использовался и при создании смеси)

       * ``kappa::models_omega model`` - модель для расчета Омега-интегралов (значение модели по умолчанию ``kappa::models_omega::model_omega_esa``):

           * ``kappa::models_omega::model_omega_rs`` - модель твердых сфер (RS)

           * ``kappa::models_omega::model_omega_vss`` - модель сфер переменного диаметра (VSS)

           * ``kappa::models_omega::model_omega_bornmayer`` - модель Борна–Майера

           * ``kappa::models_omega::model_omega_lennardjones`` - модель Леннарда–Джонса

           * ``kappa::models_omega::model_omega_esa`` - модель ESA (феноменологическая модель)

       * ``double perturbation`` - малое возмущение, используемое для избежания вырождения систем при малых (исчезающих) концентрациях компонент (значение по умолчанию - 1e-9)

.. cpp:function:: double kappa::Mixture::get_thermal_conductivity()

   Возвращает коэффициент теплопроводности (для расчета необходимо вызвать функцию ``compute_transport_coefficients``)

.. cpp:function:: double kappa::Mixture::get_shear_viscosity()

   Возвращает коэффициент сдиговой вязкости (для расчета необходимо вызвать функцию ``compute_transport_coefficients``)

.. cpp:function:: double kappa::Mixture::get_bulk_viscosity()

   Возвращает коэффициент объемной вязкости (для расчета необходимо вызвать функцию ``compute_transport_coefficients``)

.. cpp:function:: arma::vec kappa::Mixture::get_thermodiffusion()

   Возвращает вектор коэффициентов термодиффузии (для расчета необходимо вызвать функцию ``compute_transport_coefficients``).
   Сначала идут коэффициенты термодиффузии каждого колебательного уровня каждой молекулы в смеси, затем коэффициенты термодиффузии атомов в смеси,
   затем (при наличии) - электронов; коэффициенты для молекул и атомов идут в том же порядке, что и при создании смеси

